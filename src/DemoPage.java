import guru.nidi.graphviz.attribute.Color;
import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.model.LinkTarget;
import guru.nidi.graphviz.model.MutableGraph;
import guru.nidi.graphviz.model.MutableNode;
import org.apache.batik.anim.dom.SAXSVGDocumentFactory;
import org.apache.batik.bridge.*;
import org.apache.batik.gvt.GraphicsNode;
import org.apache.batik.util.XMLResourceDescriptor;
import org.w3c.dom.svg.SVGDocument;
import util.SwingUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.Scanner;

import static guru.nidi.graphviz.model.Factory.*;

public class DemoPage extends JFrame {
	private SvgPanel panelGraphPainter;
	private JPanel panelMain;
	private JTextArea inputField;
	private JButton buildGraphBtn;
	private JPanel graphDisplay;
	private JTextPane pathText;

	private WidthGraph graph = null;
	private int from, to;
	private List<WidthGraph.Path> pathList;


	private static class SvgPanel extends JPanel {
		private String svg = null;
		private GraphicsNode svgGraphicsNode = null;

		public void paint(String svg) throws IOException {
			String xmlParser = XMLResourceDescriptor.getXMLParserClassName();
			SAXSVGDocumentFactory df = new SAXSVGDocumentFactory(xmlParser);
			SVGDocument doc = df.createSVGDocument(null, new StringReader(svg));
			UserAgent userAgent = new UserAgentAdapter();
			DocumentLoader loader = new DocumentLoader(userAgent);
			BridgeContext ctx = new BridgeContext(userAgent, loader);
			ctx.setDynamicState(BridgeContext.DYNAMIC);
			GVTBuilder builder = new GVTBuilder();
			svgGraphicsNode = builder.build(ctx, doc);

			this.svg = svg;
			repaint();
		}

		@Override
		public void paintComponent(Graphics gr) {
			super.paintComponent(gr);

			if (svgGraphicsNode == null) {
				return;
			}

			double scaleX = this.getWidth() / svgGraphicsNode.getPrimitiveBounds().getWidth();
			double scaleY = this.getHeight() / svgGraphicsNode.getPrimitiveBounds().getHeight();
			double scale = Math.min(scaleX, scaleY);
			AffineTransform transform = new AffineTransform(scale, 0, 0, scale, 0, 0);
			svgGraphicsNode.setTransform(transform);
			Graphics2D g2d = (Graphics2D) gr;
			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			svgGraphicsNode.paint(g2d);
		}
	}


	public DemoPage() throws HeadlessException {
		this.setTitle("Графы");
		this.setContentPane(panelMain);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();

		graphDisplay.setLayout(new BorderLayout());
		panelGraphPainter = new SvgPanel();
		graphDisplay.add(new JScrollPane(panelGraphPainter));

		buildGraphBtn.addActionListener(e -> {
			try {
				String text = inputField.getText();
				graph = readGraph(text);

				MutableGraph renderGraph = getMutableGraph();
				displayGraph(renderGraph);
				displayPath();

			} catch (Exception ex) {
				SwingUtils.showErrorMessageBox(ex);
			}
		});


	}

	private void displayPath(){
		StringBuilder builder = new StringBuilder();

		for(WidthGraph.Path path : pathList){
			builder.append(path.toString())
					.append('\n');
		}

		pathText.setText(builder.toString());
	}

	private void displayGraph(MutableGraph graph) throws IOException {
		String renderResult = Graphviz.fromGraph(graph)
				.render(Format.SVG)
				.toString();

		panelGraphPainter.paint(renderResult);
	}

	private MutableGraph getMutableGraph() {
		MutableGraph renderGraph = mutGraph();
		int[][] adjMatrix = graph.getAdjMatrix();
		for(int i = 0; i < adjMatrix.length; i++){
			for(int j = i; j < adjMatrix.length; j++){
				if(adjMatrix[i][j] != -1){
					MutableNode viz = mutNode(Integer.toString(i));
					MutableNode childNode = mutNode(Integer.toString(j));
					viz.addLink(to(childNode).with("label",Integer.toString(adjMatrix[i][j])));
					renderGraph.add(viz);
				}
			}
		}
		return renderGraph;
	}


	public WidthGraph readGraph(String inp) {
		try {
			Scanner in = new Scanner(inp);

			int v = in.nextInt();
			int e = in.nextInt();
			int[][] adjMatrix = new int[v][v];
			for (int i = 0; i < v; i++) {
				for (int j = 0; j < v; j++) {
					adjMatrix[i][j] = -1;
				}
			}
			for (int i = 0; i < e; i++) {
				int a = in.nextInt();
				int b = in.nextInt();
				int c = in.nextInt();
				adjMatrix[a][b] = c;
				adjMatrix[b][a] = c;
			}

			from = in.nextInt();
			to = in.nextInt();

			WidthGraph widthGraph = new WidthGraph(adjMatrix);
			graph = widthGraph;
			pathList = graph.getSortedPathList(from, to);
			return widthGraph;
		} catch (Exception ex) {
			throw new IllegalArgumentException("Неверные данные");
		}

		/*List<WidthGraph.Path> pathList = graph.getSortedPathList(a, b);

		for (WidthGraph.Path path : pathList) {
			System.out.println(Arrays.toString(path.getNodeList().stream().map(WidthGraph.Node::getIndex).toArray()));
		}*/
	}
}
