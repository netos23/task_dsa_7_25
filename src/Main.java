import util.SwingUtils;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import static java.awt.Frame.MAXIMIZED_BOTH;

public class Main {

	public static void main(String[] args) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
		Locale.setDefault(Locale.ROOT);

		//SwingUtils.setLookAndFeelByName("Windows");
		//UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		//SwingUtils.setDefaultFont(null, 20);
		UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		SwingUtils.setDefaultFont("Arial", 20);

		EventQueue.invokeLater(() -> {
			try {
				JFrame mainFrame = new DemoPage();
				mainFrame.setVisible(true);
				mainFrame.setExtendedState(MAXIMIZED_BOTH);
			} catch (Exception ex) {
				SwingUtils.showErrorMessageBox(ex);
			}
		});
	}
}
