import java.util.*;

public class WidthGraph {

	private int[][] adjMatrix;
	private List<Node> nodeList;

	private List<Node> tmpPath;
	private List<List<Node>> pathList;

	public WidthGraph(int[][] adjMatrix) {
		this.adjMatrix = adjMatrix;
		nodeList = new ArrayList<>();
		for (int i = 0; i < adjMatrix.length; i++) {
			nodeList.add(new Node(i));
		}

		for (int i = 0; i < adjMatrix.length; i++) {
			for (int j = 0; j < adjMatrix[0].length; j++) {
				if (adjMatrix[i][j] != -1) {
					nodeList.get(i).neighbours.add(nodeList.get(j));
				}
			}
		}

		tmpPath = new ArrayList<>();
		pathList = new ArrayList<>();
	}


	public List<Path> getSortedPathList(int fromIndex, int toIndex) {
		pathList.clear();
		tmpPath.clear();
		for (Node node : nodeList) {
			node.visited = false;
		}

		Node from = nodeList.get(fromIndex);
		Node to = nodeList.get(toIndex);
		tmpPath.add(from);
		customDFS(from, to, -1);

		List<Path> paths = new ArrayList<>();

		for (List<Node> path : pathList) {
			Set<Node> uniqueNode = new HashSet<>(path);
			if (uniqueNode.size() != path.size()) {
				continue;
			}

			int len = 0;
			for (int i = 0; i < path.size() - 1; i++) {
				len += adjMatrix[path.get(i).index][path.get(i + 1).index];
			}
			paths.add(new Path(len, path));
		}

		Collections.sort(paths);
		return paths;
	}

	public void customDFS(Node node, Node target, int from) {

		if (node == target) {
			pathList.add(new ArrayList<>(tmpPath));
			return;
		}

		for (Node neighbour : node.neighbours) {
			if (neighbour.visited) {
				continue;
			}

			tmpPath.add(neighbour);
			neighbour.visited = true;
			customDFS(neighbour, target, node.index);

			if (!tmpPath.isEmpty()) {
				tmpPath.remove(tmpPath.size() - 1);
			}
			neighbour.visited = false;
		}
	}

	public int[][] getAdjMatrix() {
		return adjMatrix;
	}

	public List<Node> getNodeList() {
		return nodeList;
	}

	public static class Node {
		int index;
		List<Node> neighbours;
		List<Node> pathList;
		boolean isOnPath, visited;

		public Node(int index) {
			this.index = index;
			neighbours = new ArrayList<>();
			pathList = new ArrayList<>();
		}

		public int getIndex() {
			return index;
		}

		public List<Node> getNeighbours() {
			return neighbours;
		}
	}

	public static class Path implements Comparable<Path> {
		int len;
		List<Node> nodeList;

		public Path(int len, List<Node> nodeList) {
			this.len = len;
			this.nodeList = nodeList;
		}

		@Override
		public int compareTo(Path o) {
			return Integer.compare(this.len, o.len);
		}

		public int getLen() {
			return len;
		}

		public List<Node> getNodeList() {
			return nodeList;
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();

			for (Node node : nodeList) {
				builder.append(node.index).append(' ');
			}

			return "Длинна: " + len + " Путь: " + builder.toString();
		}
	}
}
